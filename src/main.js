import { createApp } from 'vue';
import App from './App.vue';
import store from './store/index.js';
import router from './router.js';
import SolutionsButton from './components/UI/buttons/SolutionsButton';
import WorksButton from './components/UI/buttons/WorksButton';
import AboutUsButton from './components/UI/buttons/AboutUsButton';
import MoreButton from './components/UI/buttons/MoreButton';
import BudgetButton from './components/UI/buttons/BudgetButton';
import BaseCard from './components/UI/cards/BaseCard';

const app = createApp(App);

app.use(router);
app.use(store);

app.component('solutions-button', SolutionsButton);
app.component('works-button', WorksButton);
app.component('about-button', AboutUsButton);
app.component('more-button', MoreButton);
app.component('budget-button', BudgetButton);
app.component('base-card', BaseCard);

app.mount('#app');
