Este proyecto es una Single Page Aplication, diseñada con Adobe xd y desarrollada con Vue.js para un negocio local y nuevo en el mercado, el cual se dedicará al mantenimiento de tipo industrial. Esto involucra tareas como electricidad, herrería, redes informáticas, entre otras, en empresas e industrias. Debido a la pandemia, en Argentina, se dicto cuarentena obligatoria hasta el mes de noviembre, por lo que las posibilidades de estos tipos de trabajos se vieron afectadas. Esto determino la interrupcion del desarrollo del proyecto habiendo concluido esta SPA. He recibido la confirmación de que el proyecto se reanudará en febrero de 2021 para continuar con lo planificado, esto es, una REST API con Node.js para implemetar metodos de pago, registro y administración de usuarios. 


This project is a Single Page Application, designed with Adobe xd and developed with Vue.js for a local business new to the market, which will be dedicated to industrial maintenance. This involves tasks such as electricity, blacksmithing, computer networks, among others, in companies and industries. Due to the pandemic, in Argentina, mandatory quarantine was issued until November, so the possibilities of these types of jobs were affected. This determined the interruption of the development of the project having concluded this SPA. I have received confirmation that the project will resume in February 2021 to continue as planned, that is, a REST API with Node.js to implement payment, registration and user management methods.

# mantenimiento-integral

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
